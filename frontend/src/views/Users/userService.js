const userService = {
  userList: [
    {
      id: 1,
      idCardCode: 1111111111111,
      name: 'Siravich',
      gender: 'M',
      address: '11/11 ม.6',
      education: 'มัธยมศึกษาตอนปลาย'
    }
  ],
  lastId: 2,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService
