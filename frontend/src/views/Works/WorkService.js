const workService = {
  workList: [
    {
      No: 1,
      type: 'full-time',
      workname: '21',
      position: 'ผู้จัดการ',
      amount: 2,
      salary: '18,000-20,000(บาท)',
      details: '111'
    }
  ],
  lastNo: 2,
  addWork(work) {
    work.No = this.lastNo++
    this.workList.push(work)
  },
  updateWork(work) {
    const index = this.workList.findIndex(item => item.No === work.No)
    this.workList.splice(index, 1, work)
  },
  deleteWork(work) {
    const index = this.workList.findIndex(item => item.No === work.No)
    this.workList.splice(index, 1)
  },
  getWorks() {
    return [...this.workList]
  }
}

export default workService
