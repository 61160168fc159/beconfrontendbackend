import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/users',
    name: 'Users',
    component: () => import('../views/Users')
  },
  {
    path: '/works',
    name: 'Works',
    component: () => import('../views/Works')
  },
  {
    path: '/compangs',
    name: '/Compangs',
    component: () => import('../views/Companys')
  },
  {
    path: '/member',
    name: 'Member',
    component: () => import('../views/Member/index.vue')
  },
  {
    path: '/login',
    name: '/Login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/register.vue')
  },
  {
    path: '/member1',
    name: '/Member1',
    component: () => import('../views/Member.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
