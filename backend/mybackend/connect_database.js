/* eslint-disable array-callback-return */
const mongoose = require('mongoose')
const User = require('./modals/User')
const Work = require('./modals/Work')
const Compang = require('./modals/Compang')

mongoose.connect('mongodb://localhost/mydb', { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

User.find(function (err, users) {
  if (err) return console.error(err)
  console.log(users)
})

Work.find(function (err, works) {
  if (err) return console.error(err)
  console.log(works)
})

Compang.find(function (err, compangs) {
  if (err) return console.error(err)
  console.log(compangs)
})
