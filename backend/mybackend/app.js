const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const usersRouter = require('./routes/users')
const workRouter = require('./routes/work')
const compangRouter = require('./routes/compangs')
const memberRouter = require('./routes/member')
const loginUserRouter = require('./routes/login')
const mongoose = require('mongoose')

const app = express()
mongoose.connect('mongodb://admin:password@localhost/mydb', {
  useNewUrlParser: true, useUnifiedTopology: true
})

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/users', usersRouter)
app.use('/works', workRouter)
app.use('/compangs', compangRouter)
app.use('/members', memberRouter)
app.use('/login', loginUserRouter)

module.exports = app
