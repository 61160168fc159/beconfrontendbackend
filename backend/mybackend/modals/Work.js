const mongoose = require('mongoose')
const Schema = mongoose.Schema
const workSchema = new Schema({
  type: String,
  workname: String,
  position: String,
  amount: String,
  salary: String,
  opendate: Date,
  closedate: Date,
  details: String
})
module.exports = mongoose.model('Work', workSchema)
