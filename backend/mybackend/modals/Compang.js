const mongoose = require('mongoose')
const Schema = mongoose.Schema
const compangSchema = new Schema({
  namecompany: String,
  gender: String,
  Natureofbusiness: String,
  Companydetails: String,
  Address: String,
  Tel: String,
  Fax: String,
  Email: String,
  Websize: String,
  Thejourney: String
})
module.exports = mongoose.model('Compang', compangSchema)
