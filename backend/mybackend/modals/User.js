const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  idCardCode: String,
  name: String,
  gender: String,
  address: String,
  education: String
})

module.exports = mongoose.model('User', userSchema)
