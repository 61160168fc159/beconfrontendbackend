const mongoose = require('mongoose')

const registerSchema = new mongoose.Schema({
  name: String,
  lastname: String,
  birthdate: String,
  gender: String,
  enlist: String,
  email: String,
  password: String,
  confirmpassword: String
})

module.exports = mongoose.model('Register', registerSchema)
