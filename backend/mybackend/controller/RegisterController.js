const Register = require('../models/Register')
const registerController = {
  /* data = [
         1
        {
            "_id" : ObjectId("60321bd9e5c1cb679b83451b"),
            "name" : "เต๋า",
            "lastname" : "คง",
            "birthdate" : "01/05/1999",
            "gender" : "ชาย",
            "enlist" : "ผ่านเกณฑ์ทหาร",
            "email" : "taogmail.com",
            "password" : "123",
            "confirmpassword" : "123"
        }
    ] */
  async addRegister (req, res) {
    const payload = req.body
    console.log(payload)
    const register = new Register(payload)
    try {
      const news = await register.save()
      res.json(news)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getRegister (req, res) {
    try {
      const register = await Register.find({})
      res.json(register)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getRegisterOne (req, res) {
    const { id } = req.params
    try {
      const register = await Register.findById(id)
      res.json(register)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = registerController
