const Work = require('../modals/Work')
const workController = {
  workList: [
    {
      No: 1,
      type: 'full-time',
      workname: '21',
      position: 'ผู้จัดการ',
      amount: 2,
      salary: '18,000-20,000(บาท)',
      details: '111'
    }
  ],
  lastNo: 2,
  async addWork (req, res) {
    const payload = req.body
    // res.json(userController.addUser(payload))
    console.log(payload)
    const work = new Work(payload)
    try {
      await work.save()
      res.json(work)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateWork (req, res) {
    const payload = req.body
    // res.json(userController.updateUser(payload))
    try {
      const work = await Work.updateOne({ _id: payload._id }, payload)
      res.json(work)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteWork (req, res) {
    const { No } = req.params
    // res.json(userController.deleteUser(id))
    try {
      const work = await Work.deleteOne({ _id: No })
      res.json(work)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getWorks (req, res) {
    try {
      const works = await Work.find({})
      res.json(works)
      console.log(works)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getWork (req, res) {
    const { No } = req.params
    try {
      const works = await Work.findById(No)
      res.json(works)
      console.log(works)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = workController
