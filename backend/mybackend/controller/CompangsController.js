const Compang = require('../modals/Compang')
const compangController = {
  userList: [
    { id: 1, namecompany: 'Pakorn', Natureofbusiness: 'รับจ้าง' }
  ],
  lastId: 3,
  async addUser (req, res) {
    const payload = req.body
    // res.json(userController.addUser(payload))
    console.log(payload)
    const compang = new Compang(payload)
    try {
      await compang.save()
      res.json(compang)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res) {
    const payload = req.body
    // res.json(userController.updateUser(payload))
    try {
      const compang = await Compang.updateOne({ _id: payload._id }, payload)
      res.json(compang)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res) {
    const { id } = req.params
    // res.json(userController.deleteUser(id))
    try {
      const compang = await Compang.deleteOne({ _id: id })
      res.json(compang)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res) {
    try {
      const compang = await Compang.find({})
      res.json(compang)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res) {
    const { id } = req.params
    try {
      const compang = await Compang.findById(id)
      res.json(compang)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = compangController
