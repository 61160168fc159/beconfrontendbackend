const UsersController = {
  userList: [
    {
      id: 1,
      idCardCode: 1111111111111,
      name: 'Siravich',
      gender: 'M',
      address: '11/11 ม.6',
      education: 'มัธยมศึกษาตอนปลาย'
    }
  ],
  lastId: 2,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
    return user
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
    return user
  },
  deleteUser (id) {
    const index = this.userList.findIndex(item => item.id === parseInt.id)
    this.userList.splice(index, 1)
    return { id }
  },
  getUsers () {
    return [...this.userList]
  },
  getUser (id) {
    const user = this.userList.find(item => item.id === parseInt(id))
    return user
  }
}

module.exports = UsersController
