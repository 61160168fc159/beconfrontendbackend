const Post = require('../modals/Work')
const postController = {
  async addPost (req, res, next) {
    const payload = req.body
    console.log(payload)
    const post = new Post(payload)
    try {
      const newpost = await post.save()
      res.json(newpost)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updatePost (req, res, next) {
    const payload = req.body
    try {
      console.log(payload)
      const post = await Post.updateOne({ _id: payload._id }, payload)
      res.json(post)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deletePost (req, res, next) {
    const { id } = req.params
    try {
      const post = await Post.deleteOne({ _id: id })
      res.json(post)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPostworks (req, res, next) {
    try {
      const posts = await Post.find({})
      res.json(posts)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPostwork (req, res, next) {
    const { id } = req.params
    try {
      const post = await Post.findById(id)
      res.json(post)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = postController
