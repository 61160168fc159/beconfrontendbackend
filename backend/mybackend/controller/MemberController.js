const Member = require('../modals/Member')
const memberController = {
  async addUser (req, res, next) {
    const payload = req.body
    console.log(payload)
    const user = new Member(payload)
    try {
      const newuser = await user.save()
      res.json(newuser)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    try {
      console.log(payload)
      const user = await Member.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await Member.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    try {
      const users = await Member.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await Member.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = memberController
