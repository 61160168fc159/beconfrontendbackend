const express = require('express')
const router = express.Router()
const compangController = require('../controller/CompangsController')

/* GET users listing. */
router.get('/', compangController.getUsers)
router.get('/:id', compangController.getUser)
router.post('/', compangController.addUser)
router.put('/', compangController.updateUser)
router.delete('/:id', compangController.deleteUser)

module.exports = router
