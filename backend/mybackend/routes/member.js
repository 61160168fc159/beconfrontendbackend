const express = require('express')
const router = express.Router()
const memberController = require('../controller/MemberController')

/* GET users listing. */
router.get('/', memberController.getUsers)

router.get('/:id', memberController.getUser)

router.post('/', memberController.addUser)

router.put('/', memberController.updateUser)

router.delete('/:id', memberController.deleteUser)

module.exports = router
