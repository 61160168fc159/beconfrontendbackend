const express = require('express')
const router = express.Router()
const LoginUserController = require('../controller/LoginService')

router.post('/register', LoginUserController.registerNewUser)
router.post('/login', LoginUserController.loginUser)

module.exports = router
