const express = require('express')
const router = express.Router()
const workController = require('../controller/WorkController')
// const User = require('../model/User');

/* GET users listing. */
router.get('/', workController.getWorks)

router.get('/:No', workController.getWork)

router.post('/', workController.addWork)

router.put('/', workController.updateWork)

router.delete('/:No', workController.deleteWork)

module.exports = router
